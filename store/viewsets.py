# -*- coding: utf-8 -*-
from uuid import UUID

from django.db.models import Max
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.pagination import PageNumberPagination
from collections import OrderedDict
from json import dumps
from base64 import b64encode

from core.utils import (
    AutoListSerializer,
    DefaultPageNumberPagination,
    LargeResultsSetPagination,
)
from .routing import endpoint, register
from . import serializers, models

class ProductJsonRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        if len(data):
            cooked = OrderedDict()
            cooked['developerName']=data[0]['developerName']
            cooked['currency']=data[0]['currency']
            for item in data:
                del item['developerName']
                del item['currency']
            cooked['products']=data
            return super(ProductJsonRenderer, self).render(
                cooked, accepted_media_type, renderer_context)
        return '{}'

class PurchasesEncodedJsonRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        if not len(data):
            data=[]
        od = OrderedDict()
        od['key'] = "MDEyMzQ1Njc4OWFiY2RlZg==\n"
        od['iv'] = "t3jir1LHpICunvhlM76edQ==\n"
        od['blob'] = b64encode(
            dumps({
                "key": "MDEyMzQ1Njc4OWFiY2RlZg==\n",
                "iv": "t3jir1LHpICunvhlM76edQ==\n",
                "blob": b64encode(dumps({"purchases":data}))
                })
        )
        return super(PurchasesEncodedJsonRenderer, self).render(
            od, accepted_media_type, renderer_context)

class AppDownloadRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        if len(data):
            od = OrderedDict()
            od['app'] = data[0]
            return super(AppDownloadRenderer, self).render(
            od, accepted_media_type, renderer_context)

@register('apps/(?P<id>.+)/download$')
class DownloadViewset(AutoListSerializer, viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.DownloadSerializer
    queryset = models.AppVersion.objects.all().order_by('-releaseTime')
    pagination_class = None
    renderer_classes = [AppDownloadRenderer,BrowsableAPIRenderer,]

    def get_queryset(self):
        id = self.kwargs['id']
        try:
            # Filter by uuid must use a valid uuid.
            # Use UUID to validate that id is a valid uuid before the actual query
            app = models.AppVersion.objects.filter(
                uuid=UUID(id)).order_by(
                    '-releaseTime')[:1]
        except ValueError:
            # (Older versions of?) OUYA will query by packageName too...
            app = models.AppVersion.objects.filter(
                app__packageName=id).order_by(
                    '-releaseTime')[:1]
        if app:
            return app



@register('appversions')
@register('appversions/(?P<packageName>.+)')
class AppVersionViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.AppVersionSerializer
    queryset = models.AppVersion.objects.all().order_by('-releaseTime')

    def get_queryset(self):
        if len(self.kwargs):
            packageName = self.kwargs['packageName']
            #game_identifier = self.request.query_params.get('only', None)
            appVersion_resultset = models.AppVersion.objects.filter(
                app__packageName=packageName
                ).order_by('-releaseTime')[:1]
            if appVersion_resultset:
                return appVersion_resultset
            else:
                return self.queryset.none()
        else:
            return models.AppVersion.objects.all().order_by('-releaseTime')

@register('details')
class DeveloperViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.DetailsSerializer
    queryset = models.App.objects.all()

@register('developers')
class DeveloperViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.DeveloperSerializer
    queryset = models.Developer.objects.all()
    lookup_field = 'uuid'
    lookup_value_regex = r'[\w-]+'

@register('developers/(?P<uuid>.+)/products/$')
class ProductViewSet(AutoListSerializer, viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.DeveloperProductsSerializer
    queryset = models.Product.objects.all()
    pagination_class = None
    renderer_classes = (ProductJsonRenderer, BrowsableAPIRenderer )

    def get_queryset(self):
        uuid = self.kwargs['uuid']
        game_identifier = self.request.query_params.get('only', None)

        developer_resultSet=models.Developer.objects.filter(uuid=uuid)
        if developer_resultSet:
            if game_identifier is not None:
                identities=game_identifier.split(",")
                products=models.Product.objects.filter(
                    developer__uuid=uuid).filter(
                    identifier__in=identities
                )
                existing_identities=[]
                for ident in products.values_list('identifier'):
                    existing_identities += ident
                new_identities=set(identities) - set(existing_identities)
                if (len(new_identities)):
                    for identity in new_identities:
                        dict=dict()
                        dict['name']=identity
                        dict['identifier']=identity
                        dict['developer']=developer_resultSet[0]
                        dict['autoGenerated']=True
                        models.Product.objects.create(**dict)
                    products=models.Product.objects.filter(
                        developer__uuid=uuid).filter(
                        identifier__in=identities
                    )
                return products
            else:
                return models.Product.objects.filter(developer__uuid=uuid)



@register('developers/(?P<uuid>.+)/gamedata')
class AppViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.GameDataSerializer
    queryset = models.App.objects.all().order_by('packageName')
    lookup_field = 'packageName'
    lookup_value_regex = '[^/]+'

    def get_queryset(self):
        uuid = self.kwargs['uuid']
        return models.App.objects.filter(developer__uuid=uuid)

@register('gamedata')
class GameDataViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.GameDataSerializer
    queryset = models.App.objects \
        .prefetch_related(
            'genres',
            'app_versions',
            'media',
            'ratings',
            'products',
            'developer',
            'players',
            'relationships',
            ) \
        .annotate(latest_releaseTime=Max('app_versions__releaseTime')) \
        .order_by('-latest_releaseTime')
    lookup_field = 'packageName'
    lookup_value_regex = '[^/]+'

@register('genres')
class GenreViewset(AutoListSerializer, viewsets.ModelViewSet):
    queryset = models.Genre.objects.order_by('genreName')
    serializer_class = serializers.GenreSerializer
    list_action_fields = ['genreName']
    pagination_class = LargeResultsSetPagination

@register('medias')
class MediaViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.MediaSerializer
    queryset = models.Media.objects.all()
    list_action_fields = ['type','url','thumb']


@register('players')
class PlayerViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.PlayerNumberSerializer
    queryset = models.PlayerNumber.objects.all()

@register('products')
class ProductViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.ProductSerializer
    queryset = models.Product.objects.all()

@register('games/(?P<packageName>.+)/purchases$')
class ProductViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.PurchaseSerializer
    queryset = models.Product.objects.all()
    pagination_class = None
    renderer_classes = (PurchasesEncodedJsonRenderer, BrowsableAPIRenderer,)

    def get_queryset(self):
        packageName = self.kwargs['packageName']
        app = models.App.objects.filter(packageName=packageName)
        if app:
            developerproducts = models.Product.objects.filter(
                developer=app[0].developer).filter(
                app__isnull=True
                )
            return app[0].products.all() | developerproducts

@register('games/(?P<packageName>.+)/purchases/decoded$')
class ProductDecodedViewset(ProductViewset):
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer, )

@register('ratings')
class RatingViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.RatingSerializer
    queryset = models.Rating.objects.all()

@register('search')
class SearchViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.SearchSerializer
    queryset = models.App.objects.all()
    pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        queryset = models.App.objects.all()
        searchterm = self.request.query_params.get('q', None)
        if searchterm is not None:
            queryset = queryset.filter(title__icontains=searchterm)
        return queryset

@register('sessions')
class SessionViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.SessionSerializer
    queryset = models.Session.objects.all()

@register('users')
class UserViewset(AutoListSerializer, viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    queryset = models.User.objects.all()
