#! /usr/bin/env python

from __future__ import division, print_function, unicode_literals

import argparse
import errno
import os
import pty
import select
import subprocess as sp
import sys
import textwrap
from enum import Enum

import toml

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class Status(Enum):
    OK = '\033[92m'
    WARN = '\033[93m'
    FAIL = '\033[91m'
    RESET = '\033[0m'

    def __str__(self):
        return self.value

    def __call__(self, message):
        return self + message + Status.RESET

    def __add__(self, other: str):
        return str(self) + other

    def __radd__(self, other: str):
        return other + str(self)


GLYPHS = (Status.OK('✓'), Status.FAIL('✗'))


class GithooksCLI:
    """Interface for installing, uninstalling, configuring, and running git hooks"""

    # TODO: Support other optional config files, e.g., setup.cfg, .githooks.ini, .githooks.json, etc.
    CONFIG_FILE = 'pyproject.toml'
    SUPPORTED_HOOKS = [
        'applypatch-msg',
        'commit-msg',
        'post-applypatch',
        'post-checkout',
        'post-commit',
        'post-merge',
        'post-receive',
        'post-rewrite',
        'post-update',
        'pre-applypatch',
        'pre-auto-gc',
        'pre-commit',
        'pre-push',
        'pre-rebase',
        'pre-receive',
        'prepare-commit-msg',
        'update',
    ]

    def __init__(self):
        self.project_root = self._project_root()
        self.config = self._config()
        self.args = self._args()

        if self.args.command in self.SUPPORTED_HOOKS:
            self._run_hook(self.args.command, self.args.task, verbose=self.args.verbose)
        else:
            getattr(self, self.args.command)()

    def _run_hook(self, hook, task, verbose=False):
        os.chdir(self.project_root)
        tasks = self.config.get(hook, {})
        for task, command in [(task, tasks.get(task))] if task else tasks.items():
            returncode, out = self._run(command)
            print('  [ {} ] {}{}'.format(GLYPHS[not not returncode], task, (': ' + command) if verbose else ''))
            if returncode or verbose:
                for line in out.splitlines():
                    print(' ' * 7, line)

    def install(self):
        # TODO: Make this calculate relative path to location of this file from project_root
        self._run('git config core.hooksPath .githooks')

        for hook in self.config.keys():
            self._symlink(os.path.basename(__file__), os.path.join(BASE_DIR, hook), self.args.force)

        if self.args.clean:
            for filename in os.listdir(BASE_DIR):
                if filename in self.SUPPORTED_HOOKS and filename not in self.config:
                    os.remove(os.path.join(BASE_DIR, filename))

    def uninstall(self):
        self._run('git config --unset core.hooksPath')

        for hook in self.config.keys():
            os.remove(os.path.join(BASE_DIR, hook))

    def _args(self):
        class CombinedHelpFormatter(argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter):
            pass

        parser = argparse.ArgumentParser(
            description=textwrap.dedent(self.__class__.__doc__), formatter_class=CombinedHelpFormatter
        )

        filename = os.path.basename(__file__)
        if filename in self.SUPPORTED_HOOKS:
            if filename not in self.config:
                parser.exit()

            self._args_tasks(parser, self.config[filename])
        else:
            self._args_commands(parser)

        args = parser.parse_args()

        if filename in self.SUPPORTED_HOOKS:
            args.command = filename

        return args

    def _args_commands(self, parser):
        sp = parser.add_subparsers(dest='command')

        install = sp.add_parser('install', help='Create git hook files')
        install.add_argument(
            '-c', '--clean', action='store_true', help="Remove hooks that don't have a definition in the config file"
        )
        install.add_argument('-f', '--force', action='store_true', help='Overwrite existing hooks')

        sp.add_parser('uninstall', help='Delete git hook files defined in the config file')

        for hook, tasks in self.config.items():
            h = sp.add_parser(hook, help=None)
            self._args_tasks(h, tasks)

    def _args_tasks(self, parser, tasks):
        parser.add_argument('-v', '--verbose', action='store_true', help='Show additional output')

        sp = parser.add_subparsers(dest='task')
        for task, command in tasks.items():
            sp.add_parser(task, help=command)

    def _config(self):
        contents = toml.load(os.path.join(self.project_root, self.CONFIG_FILE)).get('githooks', {})
        return {k: v for k, v in contents.items() if k in self.SUPPORTED_HOOKS}

    def _project_root(self):
        return self._find_file_upwards(self.CONFIG_FILE)

    @staticmethod
    def _find_file_upwards(filename):
        def path_generator(start):
            yield start
            while start != os.path.dirname(start):
                start = os.path.dirname(start)
                yield start

        for dirpath in path_generator(BASE_DIR):
            if filename in os.listdir(dirpath):
                return dirpath

    @staticmethod
    def _run(cmd):
        def read_all_from_file_descriptor(fd, buffer=4096):
            fo = os.fdopen(fd)
            chunks = []
            while select.select([fo], [], [], 0)[0]:
                try:
                    data = os.read(fd, buffer)
                    data_len = len(data)
                    data = data.decode(sys.stdout.encoding)
                except OSError as e:
                    if e.errno == errno.EIO:
                        break
                    raise
                else:
                    chunks.append(data)
                    if data_len < buffer:
                        break
            fo.close()
            return ''.join(chunks).strip()

        master, slave = pty.openpty()
        child = sp.Popen(cmd.strip(), stdout=slave, stderr=sp.STDOUT, shell=True)
        child.communicate()
        out = read_all_from_file_descriptor(master)
        os.close(slave)
        return child.returncode, out

    @staticmethod
    def _symlink(src, dst, force=False):
        try:
            os.symlink(src, dst)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

            if force:
                os.remove(dst)
                os.symlink(src, dst)
            else:
                print("File exists: '{}' (use --force to overwrite)".format(dst))


if __name__ == "__main__":
    GithooksCLI()
