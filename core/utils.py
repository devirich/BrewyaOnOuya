# -*- coding: utf-8 -*-
import os
from functools import wraps

from django.conf.urls import url
from django.db import DEFAULT_DB_ALIAS, connections
from django.db.migrations.executor import MigrationExecutor
from rest_framework.pagination import PageNumberPagination
from django.http import FileResponse
from django.conf import settings


def is_database_synchronized(database=DEFAULT_DB_ALIAS):
    """Determine if database has unapplied migrations"""
    connection = connections[database]
    connection.prepare_database()
    executor = MigrationExecutor(connection)
    targets = executor.loader.graph.leaf_nodes()
    return False if executor.migration_plan(targets) else True


def json_urls(responses):
    """Dynamically generates urlpatterns mapping to a collection of JSON responses"""
    return [url(route + '$', simple_json_response(endpoint), name=endpoint) for route, endpoint in responses.items()]


def memoprop(func):
    """
    Creates a memoized property out of a method

    Memoization caches the result of calling a function, and uses the cache to supply the same result later
    A property is a method that acts like an attribute, where you don't have to "call" it with parenthesis.

    Since a property takes no arguments, a memoization cache only needs to hold one value.

    The value of this is that you don't have to initialize values you aren't going to need, nor keep track of
    what has been initialized.  Just supply the formula for calculating some expensive value in a function,
    access it when you need it, and this decorator will keep everything efficient.
    """

    @wraps(func)
    def _inner(self):
        var_name = '_{}'.format(func.__name__)
        if not hasattr(self, var_name):
            setattr(self, var_name, func(self))

        return getattr(self, var_name)

    return property(_inner)


class StaticEndpointResolver(object):
    def get_filename(self, endpoint, query_dict):
        if endpoint == 'v1/details':
            endpoint += '/app=' + query_dict.get('app')
        return os.path.join(settings.BASE_DIR, 'store', 'responses', '{}.json'.format(endpoint))

    def simple_json_response(self, endpoint):
        """Dynamically generates view functions that load and return a response from a JSON file on disk"""
        from django.conf import settings

        def _inner(request):
            filename = self.get_filename(endpoint, request.GET)
            return FileResponse(open(filename, 'rb'), content_type='application/json')

        return _inner

    @property
    def urls(self):
        """Dynamically creates filepath mapping to the contents of JSON files"""
        path = settings.BASE_DIR + '/store/responses'
        static_responses = set()
        for root, dirnames, filenames in os.walk(path):
            for file in filenames:
                endpoint = os.path.splitext(os.path.relpath(os.path.join(root, file), start=path))[0]
                if '=' in endpoint:
                    endpoint, _, _ = endpoint.rpartition('/')

                static_responses.add(endpoint)

        return [url(path + '$', self.simple_json_response(path), name=path) for path in static_responses]


class AutoListSerializer:
    def list_serializer(self):
        if hasattr(self, 'list_action_serializer_class'):
            return self.list_action_serializer_class

        if hasattr(self, 'list_action_fields'):
            Meta = type('Meta', (self.serializer_class.Meta, object), {'fields': self.list_action_fields})
            serializer_class = type('AutoListSerializer', (self.serializer_class, object), {'Meta': Meta})
            return serializer_class

        if hasattr(self, 'list_action_excludes'):
            current_fields = self.serializer_class().fields
            fields = tuple(set(current_fields) - set(self.list_action_excludes))
            Meta = type('Meta', (self.serializer_class.Meta, object), {'fields': fields})
            serializer_class = type('AutoListSerializer', (self.serializer_class, object), {'Meta': Meta})
            return serializer_class

        return self.serializer_class

    def get_serializer_class(self):
        if self.action == 'list':
            return self.list_serializer()
        return self.serializer_class

class DefaultPageNumberPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 10000

class LargeResultsSetPagination(DefaultPageNumberPagination):
    page_size = 1000
