from django.conf.urls import include, url

from core import utils
from . import routing, views, viewsets


urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^', include(routing.urlpatterns)),
    url(r'^api/', include(utils.StaticEndpointResolver().urls)),
    url(r'^api/v1/', include(routing.router.urls)),
]
