# -*- coding: utf-8 -*-
from rest_framework import serializers
from collections import OrderedDict
import uuid
from time import time
from json import dumps

from base64 import b64encode
from .routing import endpoint, register
from . import models
#Products serializer
#PlayerNumbers serializer
###########################################################
################# BUILDING BLOCKS######## #################
###########################################################

# def to_representation(self, instance):
#     REQUIRED_FIELDS = ('')
#     result = super(____Serializer, self).to_representation(instance)
#     return OrderedDict([(key, result[key]) for key in result if
#         result[key] is not None or
#         key in REQUIRED_FIELDS
#         ])

###########################################################
################# INDEPENDENT SERIALIZERS #################
###########################################################

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = '__all__'

###########################################################
################## DEPENDENT SERIALIZERS ##################
###########################################################
class AppVersionAppsSerializer(serializers.ModelSerializer):
    # md5sum = serializers.CharField(source='apk.md5sum',read_only=True)

    class Meta:
        model = models.AppVersion
        fields = ['releaseTime', 'uuid']

class DeveloperAppsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Developer
        fields = ['supportEmail','supportPhone','founder','name']

class GenreAppsSerializer(serializers.ModelSerializer):
    #filepickerScreenshots
    class Meta:
        model = models.Genre
        fields = '__all__'

class MediaAppsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Media
        fields = "__all__"

class RatingAppsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Rating
        fields = ['likeCount','rating','reviewCount']

class AppAppsSerializer(serializers.ModelSerializer):
    app_version = AppVersionAppsSerializer()
    developer = DeveloperAppsSerializer()
    rating = RatingAppsSerializer()
    filePickerScreenshots = MediaAppsSerializer(many=True)
    gamerNumbers = serializers.CharField(source='playerNumbers')
    mainImageFullUrl = serializers.CharField(source='titleImage')
    mobileAppIcon = serializers.CharField(source='iconImage')
    genres = GenreAppsSerializer(many=True)

    class Meta:
        model = models.App
        fields = ['uuid','title','developer','website','overview',
        'contentRating','gamerNumbers','description',
        'app_version','developer','genres','filePickerScreenshots','rating'
        ]

class AppVersionSerializer(serializers.ModelSerializer):
    # app = serializers.PrimaryKeyRelatedField(read_only=True)
    uuid = serializers.CharField(required=False)
    date = serializers.DateTimeField(source='releaseTime')
    versionCode = serializers.IntegerField(required=False, allow_null=True,)
    cert_fingerprint = serializers.CharField(required=False, source='certFingerprint')
    cert_subject = serializers.CharField(required=False,source='certSubject')

    class Meta:
        model = models.AppVersion
        # fields = '__all__'
        # exclude = ['id','app']
        fields = [
                'name',
                'versionCode',
                'uuid',
                'date',
                'url',
                'size',
                'md5sum',
                'publicSize',
                'nativeSize',
                'stability',
                'cert_fingerprint',
                'cert_subject',
                ]

    def to_representation(self, instance):
        REQUIRED_FIELDS = [
                    "name",
                    "versionCode",
                    "uuid",
                    "date",
                    "url",
                    "size",
                    "md5sum",
                ]
        result = super(AppVersionSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if
            result[key] is not None or
            key in REQUIRED_FIELDS
            ])
    # def create(self, validated_data):
    #     appversion = models.AppVersion.objects.create(**validated_data)
    #     return appversion

    # def update(self, inst, validated_data):
    #     vars(inst).update(validated_data)
    #     inst.save()
    #     return inst


class DetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.App
        fields = '__all__'

class DownloadSerializer(serializers.ModelSerializer):
    fileSize = serializers.ReadOnlyField(source='size',)
    version = serializers.ReadOnlyField(source='uuid',)
    downloadLink = serializers.ReadOnlyField(source='url')
    contentRating = serializers.SlugRelatedField(
        slug_field='contentRating',
        source='app',
        queryset=models.App.objects
    )

    class Meta:
        model = models.AppVersion
        fields = [
            'fileSize',
            'version',
            'contentRating',
            'downloadLink',
        ]

class DeveloperSerializer(serializers.ModelSerializer):
    uuid = serializers.CharField(required=False, allow_null=True)
    supportPhone = serializers.CharField(required=False, allow_null=True,)
    alternateEmail = serializers.CharField(required=False, allow_null=True,)

    class Meta:
        model = models.Developer
        fields = [
            'uuid','name',
            # 'website','donationUrl',
            'supportEmail','alternateEmail',
            'supportPhone','founder',
            ]

    def to_representation(self, instance):
        REQUIRED_FIELDS = [
            'uuid',
            'name',
            'supportEmail',
            'supportPhone',
            'founder',
            ]
        result = super(DeveloperSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if
            result[key] is not None or
            key in REQUIRED_FIELDS
            ])


class DeveloperProductsSerializer(serializers.ModelSerializer):
    type = serializers.ReadOnlyField(default='entitlement')
    percentOff = serializers.ReadOnlyField(default=0)
    localPrice = serializers.ReadOnlyField()
    priceInCents = serializers.ReadOnlyField(default=0,)
    developerName=serializers.SlugRelatedField(
        slug_field='name',
        source='developer',
        queryset=models.Developer.objects
    )

    class Meta:
        model = models.Product
        fields = ['type', 'description', 'percentOff',
                  'originalPrice', 'localPrice', 'priceInCents',
                  'name', 'identifier',
                  'developerName',
                  'currency',
                  ]



class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Genre
        fields = '__all__'

class MediaSerializer(serializers.ModelSerializer):
    thumb=serializers.CharField(required=False,)

    class Meta:
        model = models.Media
        # fields = '__all__'
        # fields = ['type','url','thumb']
        fields = ['type','url','thumb']

    def to_representation(self, instance):
        REQUIRED_FIELDS = ['type','url',]
        result = super(MediaSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if
            result[key] is not None or
            key in REQUIRED_FIELDS
            ])

class PlayerNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PlayerNumber
        fields = '__all__'

class PurchaseSerializer(serializers.ModelSerializer):
    purchaseDate  = serializers.IntegerField(read_only=True, default=time(),)
    # generateDate  = serializers.IntegerField(read_only=True, default=time(),)
    # identifier = serializers.CharField(source="")
    gamer = serializers.CharField(read_only=True, default="BrewyaUser")
    uuid = serializers.CharField(read_only=True, default="f791f35c-697c-4d4b-8298-783e70622ef2")
    priceInCents = serializers.IntegerField(read_only=True, default=123)
    localPrice = serializers.FloatField(read_only=True, default=float(1.23))

    class Meta:
        model = models.Product
        fields = [
            'purchaseDate',
            # 'generateDate',
            'identifier',
            'gamer','uuid',
            'priceInCents',
            'localPrice'
            # ,'currency',
        ]

    def create(self, validated_data):
        return "{}"

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Product
        fields = [
            'promoted','identifier','name','description',
            'localPrice','originalPrice','currency',
        ]

    def to_representation(self, instance):
        # REQUIRED_FIELDS = ['']
        result = super(ProductSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if
            result[key] is not None
            # or key in REQUIRED_FIELDS
            ])

class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Rating
        fields = ['likeCount','average','ratingCount']
        fields = '__all__'

class SearchSerializer(serializers.ModelSerializer):
    url=serializers.SerializerMethodField()

    def get_url(self, obj):
        return 'ouya://launcher/details?app={}'.format(obj.packageName)

    class Meta:
        model = models.App
        fields = ['title','url','contentRating']

class SessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Session
        fields = '__all__'

class AppSerializer(serializers.ModelSerializer):
    developer = DeveloperAppsSerializer()
    media = MediaAppsSerializer(many=True)
    app_versions = AppVersionSerializer(many=True)
    class Meta:
        model = models.App
        fields = "__all__"
        # fields = ['title', 'developer', 'packageName']

class GameDataRelationshipSerializer(serializers.ModelSerializer):
    unlocked=serializers.CharField(required=False)
    original=serializers.CharField(required=False)

    class Meta:
        model = models.Relationship
        fields = ['unlocked','original',]
        # fields = '__all__'

    def to_representation(self, instance):
        # REQUIRED_FIELDS = ['']
        result = super(GameDataRelationshipSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if
            result[key] is not None
            # or key in REQUIRED_FIELDS
            ])

class GameDataRatingSerializer(serializers.ModelSerializer):
    average = serializers.DecimalField(source='rating',
        max_digits=10, decimal_places=2, default=0.00)
    count = serializers.IntegerField(source='ratingCount')

    class Meta:
        model = models.Rating
        fields = ['likeCount','average','count',]

    def to_representation(self, manager):
        if len(manager.all()):
            total_count, total_rating, like_count = 0, 0, 0
            for count, rating, likes in manager.values_list('ratingCount', 'rating', 'likeCount'):
                total_count += count
                total_rating += rating * count
                like_count += likes
            od = OrderedDict()
            od['likeCount'] = like_count
            if total_count and total_rating:
                od['average']   = round(total_rating / total_count, 2)
            else:
                od['average'] = 0.00
            od['count']     = total_count
            return od

class GameDataSerializer(serializers.ModelSerializer):
    media = MediaSerializer(many=True, required=True)
    developer = DeveloperSerializer()
    players = serializers.SlugRelatedField(
        many=True,
        required=False,
        slug_field='playerCount',
        queryset=models.PlayerNumber.objects
    )
    rating = GameDataRatingSerializer(required=False, source="ratings")
    products = ProductSerializer(required=False, many=True,)
    discover = serializers.URLField(allow_null=True, required=False)
    genres = serializers.SlugRelatedField(
        many=True,
        slug_field='genreName',
        queryset=models.Genre.objects

    )

    releases = AppVersionSerializer(many=True, source="app_versions")
    relationships = GameDataRelationshipSerializer(required=False)

    class Meta:
        model = models.App
        fields = [
            # 'id',
            'packageName',
            'title',
            'description',
            'players',
            'genres',
            'releases',
            'discover',
            'media',
            'developer',
            'contentRating',
            'website',
            'firstPublishedAt',
            'inAppPurchases',
            'overview',
            'premium',
            'rating',
            'products',
            'relationships',
            ]
        # fields = '__all__'
        # exlcude = ['id']

    def to_representation(self, instance):
        REQUIRED_FIELDS = [
            'packageName',
            'title',
            'genres',
            'releases',
            'discover',
            'media',
            'developer',
        ]
        result = super(GameDataSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key])
            for key in result
                if
                    key in REQUIRED_FIELDS
                    # We only want to remove fields if None (Null) or Empty array or not in REQUIRED_FIELDS
                    or (
                        result[key] is not None
                        and not (type(result[key]) is list and not result[key])
                        )
            ])

    def create(self, validated_data):

        if 'ratings' in validated_data:
            rating = validated_data.pop('ratings')
        if 'media' in validated_data:
            media = validated_data.pop('media')
        app_versions = validated_data.pop('app_versions')

        developer = validated_data.pop('developer')
        if models.Developer.objects.filter(uuid=developer['uuid']):
            print("Matching dev found!",developer['uuid'])
            developer = models.Developer.objects.filter(uuid=developer['uuid'])[0]
        else:
            if developer['uuid'] is None:
                developer['uuid'] = uuid.uuid4()
            developer = models.Developer.objects.create(**developer)

        genres = validated_data.pop('genres')
        players = validated_data.pop('players')
        if 'relationships' in validated_data:
            relationships = validated_data.pop('relationships')
        if 'products' in validated_data:
            products = validated_data.pop('products')

        app = models.App.objects.create(
            developer=developer,
            **validated_data
        )
        app.save()

        if 'rating' in locals():
            models.Rating.objects.create(app=app, **rating)
        if 'media' in locals():
            for asset in media:
                models.Media.objects.create(app=app, **asset)
        if 'app_versions' in locals():
            for version in app_versions:
                models.AppVersion.objects.create(app=app, **version)
        if 'genres' in locals():
            app.genres.set(genres)
            # for genre in genres:
            #     models.Genre.objects.create(app=app, genreName=genre)
        if 'players' in locals():
            app.players.set(players)
        if 'relationships' in locals():
            # for relation in relationship:
                # obj = relation.popitem()
            models.Relationship.objects.create(app=app, **relationships)
        if 'products' in locals():
            for product in products:
                models.Product.objects.create(app=app, developer=developer, **product)

        return app

    def update(self, inst, validated_data):
        # Be sure to ignore any updates to rating(s) to avoid double-vote issue
        # use a compare instead of .Delete() someday.
        if 'media' in validated_data:
            media = inst.media
            media.all().delete()
            for item in validated_data.pop('media'):
                media.create(app=inst, **item)
        if 'app_versions' in validated_data:
            # Add new version, do not delete old versions.
            app_versions = inst.app_versions
            for item in validated_data.pop('app_versions'):
                if not app_versions.all().filter(uuid=item['uuid']):
                    app_versions.create(app=inst, **item)
        if 'genres' in validated_data:
            genres = inst.genres
            genres.clear()
            for item in validated_data.pop('genres'):
                genres.add(item.id)
        if 'players' in validated_data:
            players = inst.players
            players.clear()
            for item in validated_data.pop('players'):
                players.add(item.id)
        if 'products' in validated_data:
            products = inst.products
            products.all().delete()
            for item in validated_data.pop('products'):
                products.create(app=inst, **item)
        if 'relationships' in validated_data:
            relationships = inst.relationships
            vars(relationships).update(validated_data.pop('relationships'))
            relationships.save()
        if 'developer' in validated_data:
            developer = inst.developer
            vars(developer).update(validated_data.pop('developer'))
            developer.save()
        vars(inst).update(validated_data)
        inst.save()
        return inst
