from functools import wraps

from django.conf.urls import url
from django.http import HttpResponseNotAllowed
from rest_framework import serializers, viewsets


def methods(*allowed_methods):
    """If the request method is one of the supplied allowed methods, call proceeds.  Otherwise 404 is raised"""

    def _outer(f):
        if not allowed_methods:
            return f

        @wraps(f)
        def _inner(request):
            if request.method in allowed_methods:
                return f(request)
            return HttpResponseNotAllowed(allowed_methods)

        return _inner

    return _outer


_methods = methods


def router_registrar(router):
    def register(route_name):
        """Automatically adds viewsets to a `rest_framework.DefaultRouter`"""

        def _inner(viewset):
            router.register(route_name, viewset)
            return viewset

        return _inner

    return register


def route_registrar(patterns):
    def route(location, methods=()):
        """
        Flask-style view routing in Django!  Just decorate your view function with this and give it a route

        urlpatterns automatically get updated with anything registered by this mechanism
        """

        def _inner(f):
            f = _methods(*methods)(f)
            patterns.append(url(location + '$', f, name=f.__name__))
            return f

        return _inner

    return route


def endpoint_registrar(register):
    def endpoint(endpoint_name):
        """
        Dynamically generates a Django REST Framework endpoint

        Takes a Django model, and dynamically generates a
        * `rest_framework.ModelSerializer` and a
        * `rest_framework.ModelViewSet`
        to go along with it
        """

        def _inner(model_class):
            Meta = type('Meta', (), {'model': model_class, 'fields': '__all__'})
            model_class.serializer = type(
                model_class.__name__ + 'Serializer', (serializers.ModelSerializer,), {'Meta': Meta}
            )

            local_vars = {'queryset': model_class.objects.all(), 'serializer_class': model_class.serializer}
            model_class.viewset = register(endpoint_name)(
                type(model_class.__name__ + 'ViewSet', (viewsets.ModelViewSet,), local_vars)
            )

            return model_class

        return _inner

    return endpoint
