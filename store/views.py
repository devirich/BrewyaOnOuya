# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import uuid

from django.http import HttpResponse, JsonResponse

from store.routing import route

@route('^$')
def root(request):
    return HttpResponse(status=204)

@route('status')
def status(request):
    return HttpResponse(status=204)

@route('^api/v1/crash_report$')
def root(request):
    return HttpResponse(status=200)

@route('^api/v1/survey_answers$')
def root(request):
    return HttpResponse(status=200)

@route('api/v1/sessions', methods=['POST'])
def sessions(request):
    return JsonResponse({'token': uuid.uuid4()})

@route('api/v1/events', methods=['POST'])
def sessions(request):
    return JsonResponse({})

@route('api/v1/gamers/me/consoles', methods=['GET', 'POST', 'PUT'])
def consoles(request):
    return JsonResponse({})

@route('api/v1/gamers/key', methods=['POST', 'PUT'])
def key(request):
    return JsonResponse({}, status=201)

@route('api/v1/premium_purchases', methods=['GET'])
def premium_purchases(request):
    return JsonResponse({'games': []})
