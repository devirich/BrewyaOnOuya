from rest_framework.routers import DefaultRouter

from core.routing import endpoint_registrar, route_registrar, router_registrar

urlpatterns = []
router = DefaultRouter(trailing_slash=False)
# router = DefaultRouter()

route = route_registrar(urlpatterns)
register = router_registrar(router)
endpoint = endpoint_registrar(register)
